import 'package:app/addSession.dart';
import 'package:app/sessionList.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized;

  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyBZlDc59-CjIQcPAuLIfTB7N6Nca-fHdyo",
          authDomain: "my-web-app-73041.firebaseapp.com",
          projectId: "my-web-app-73041",
          storageBucket: "my-web-app-73041.appspot.com",
          messagingSenderId: "694320062689",
          appId: "1:694320062689:web:b368c4c8b34412fa1d57a0",
          measurementId: "G-YRWYREBNFT"));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Center(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Thikamisava Bokings',
        theme: ThemeData(
          primarySwatch: Colors.pink,
        ),
        home: const MyHomePage(title: 'welcome to booking site'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[AddSession(), sessionList()],
        ),
      ),
    );
  }
}
