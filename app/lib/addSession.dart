import 'dart:developer';

import 'package:app/sessionList.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AddSession extends StatefulWidget {
  const AddSession({Key? key}) : super(key: key);

  @override
  State<AddSession> createState() => _AddSessionState();
}

class _AddSessionState extends State<AddSession> {
  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController surnameController = TextEditingController();
    TextEditingController roomController = TextEditingController();

    Future _addSession() {
      final name = nameController.text;
      final surname = surnameController.text;
      final room = roomController.text;

      final ref = FirebaseFirestore.instance.collection("addSession").doc();

      return ref
          .set({
            "Name": name,
            "surname": surname,
            "Room": room,
            "doc_id": ref.id,
          })
          .then((value) => {
                nameController.text = "",
                surnameController.text = "",
                roomController.text = "",
              })
          .catchError((onError) => log(onError));
    }

    return Column(
      children: [
        Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                controller: nameController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular((20))),
                    hintText: "Enter Name"),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                controller: surnameController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular((20))),
                    hintText: "Enter Surname"),
              ),
            ),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: TextField(
                  controller: roomController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular((20))),
                      hintText: "Enter Room"),
                )),
            ElevatedButton(
                onPressed: () {
                  _addSession();
                },
                child: const Text("Add Session"))
          ],
        ),
      ],
    );
    sessionList();
  }
}
