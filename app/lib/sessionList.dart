// ignore_for_file: dead_code

import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:app/addSession.dart';

// ignore: camel_case_types
class sessionList extends StatefulWidget {
  const sessionList({Key? key}) : super(key: key);

  @override
  State<sessionList> createState() => _sessionListState();
}

// ignore: camel_case_types
class _sessionListState extends State<sessionList> {
  final Stream<QuerySnapshot> _mySessions =
      FirebaseFirestore.instance.collection("sessions").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController _nameFieldCtrl = TextEditingController();
    TextEditingController _surnameFieldCtrl = TextEditingController();
    TextEditingController _roomFieldCtrl = TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance
          .collection("sessions")
          .doc(docId)
          .delete()
          .then((value) => print("deleted"));
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection("sessions");
      _nameFieldCtrl.text = data["Name"];
      _nameFieldCtrl.text = data["Surname"];
      _nameFieldCtrl.text = data["Room"];

      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: const Text("Update"),
          content: Column(mainAxisSize: MainAxisSize.min, children: [
            TextField(
              controller: _nameFieldCtrl,
            ),
            TextField(
              controller: _nameFieldCtrl,
            ),
            TextField(
              controller: _nameFieldCtrl,
            ),
            TextButton(
                onPressed: () {
                  collection.doc(data["doc_id"].update({
                    "Name": _nameFieldCtrl.text,
                    "Surname": _surnameFieldCtrl.text,
                    "Room": _roomFieldCtrl.text,
                  }));
                  Navigator.pop(context);
                },
                child: const Text("Update"))
          ]),
        ),
      );
    }

    return StreamBuilder(
      stream: _mySessions,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return const Text("Somethings went wrong");
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }

        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                  child: SizedBox(
                      height: (MediaQuery.of(context).size.height),
                      width: MediaQuery.of(context).size.width,
                      child: ListView(
                        children: snapshot.data!.docs
                            .map((DocumentSnapshot documentSnapshot) {
                          Map<String, dynamic> data =
                              documentSnapshot.data()! as Map<String, dynamic>;

                          return Column(
                            children: [
                              Card(
                                child: Column(
                                  children: [
                                    ListTile(
                                      title: Text(data['Name']),
                                      subtitle: Text(data['Surname']['Room']),
                                    ),
                                    ButtonTheme(
                                        child: ButtonBar(
                                      children: [
                                        OutlinedButton.icon(
                                          onPressed: () {
                                            _update(data);
                                          },
                                          icon: const Icon(Icons.edit),
                                          label: const Text("Edit"),
                                        ),
                                        OutlinedButton.icon(
                                          onPressed: () {
                                            _delete(data["doc_id"]);
                                          },
                                          icon: const Icon(Icons.remove),
                                          label: const Text("Delete"),
                                        )
                                      ],
                                    ))
                                  ],
                                ),
                              )
                            ],
                          );
                        }).toList(),
                      )))
            ],
          );
        } else {
          return (const Text("No data"));
        }
      },
    );
  }
}
